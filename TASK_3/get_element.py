from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from base_element import BaseElement

host = "https://ptable.com/?lang=ru"
locator_elements = ("xpath", "//ol[@id='Ptable']/li")
driver = webdriver.Chrome()
driver.maximize_window()

driver.get(host)

wait = WebDriverWait(driver, 10)
elements = wait.until(EC.presence_of_all_elements_located(locator_elements))

for element in range(len(elements)):
    number, name, full_name_ru, weight = elements[element].text.split("\n")
    class_instance = BaseElement(number, full_name_ru, weight)
    print(f"Номер элемента: {class_instance.atomic}, название элемента: {class_instance.name},"
          f" атоманая масса элемента: {class_instance.weight}")
